# -*- coding: utf-8 -*-

class converter:
	def __init__(self):
		self.hundreds_words = [u'сто', u'двести', u'триста', u'четыреста', u'пятьсот', u'шестьсот', 
													 u'семьсот', u'восемьсот', u'девятьсот']

		self.dozens_words = {'90': u'девяносто',	'80': u'восемьдесят',	'70': u'семьдесят',
                         '60': u'шестьдесят',	'50': u'пятьдесят',		'40': u'сорок',
                         '30': u'тридцать',		'20': u'двадцать'}

		self.low_words = ['ноль', u'один', u'два', u'три', u'четыре', u'пять', u'шесть', 
											u'семь', u'восемь', u'девять', u'десять', u'одинадцать',
											u'двенадцать', u'тринадцать', u'четырнадцать', u'пятнадцать',
											u'шестнадцать', u'семнадцать', u'восемнадцать', u'девятнадцать']

		# too bib difference - total substitute number + rank
		self.thousand_alternative_words = {'1': u'одна тысяча', 
																			 '2': u'две тысячи', 
																			 '3': u'три тысячи', 
																			 '4': u'четыре тысячи'}

		# Only defference is word ending
		self.high_ranks = [u'тысяч', u'миллион', u'миллиард', u'триллион']

		self.high_ranks_default_end = u'ов'
		self.high_ranks_alternative_ends = {'1': u'', '2': u'а', '3': u'а', '4': u'а'}

	def convert(self, string):
		out = ''

		while not (len(string) - 1) / 3  == 0:
			rank =  (len(string) - 1 ) / 3 
			count = len(string) - rank*3

			out += ' ' + self.parse(string[:count], rank)
			string = string[count:]

		out += ' ' + self.parse(string, 0)
		out = out.replace('  ', ' ')

		return out


	# input 121 345
	# part = 12, rank = 2
	def parse(self, part, rank):
		print 'parse: part: %s, rank: %s' % (part, rank)
		out = ''

		if len(part) == 1:
			out += self.parse_last_digit( part, rank )
			return out

		if len(part) == 3 and part[0] != '0':
			num = int(part[0])
			out += self.hundreds_words[num - 1] + ' '

		if len(part) >= 2:
			out += self.parse_dosens( part, rank )
		else:
			out += self.parse_last_digit( part, rank )

		return out

	def parse_dosens(self, part, rank):
		print 'parse_dosens: part: %s, rank: %s' % (part, rank)
		out = ''
		num = int(part[-2:])

		if num < 20:
			if not part[-2] == '0':
				out += self.low_words[int( part[-2:] )]
				if rank != 0 and int(part) != 0: 
					out += ' ' + self.high_ranks[rank - 1]
			else:
				out += self.parse_last_digit( part, rank )
		else:
			dosens = part[-2]+'0'
			out += self.dozens_words[ dosens ]
			out += ' ' + self.parse_last_digit( part, rank )

		return out


	def parse_last_digit(self, part, rank):
		out = ''

		if rank == 0:
			if not part[-1] == '0': out += self.low_words[int( part[-1] )]
		elif rank == 1:
			out += self.parse_thousands_last_digit(part)
		else:
			out += self.parse_high_ranks_last_digit(part, rank)

		return out

	def parse_thousands_last_digit(self, part):
		out = ''

		if not part[-1] in self.thousand_alternative_words:
			if not part[-1] == '0':	out += self.low_words[int( part[-1] )]
			if not int(part) == 0:	out += ' ' + self.high_ranks[0]
		else:
			out += self.thousand_alternative_words[ part[-1] ]
		
		return out

	def parse_high_ranks_last_digit(self, part, rank):
		print 'parse_high_ranks_last_digit: digit: %s, rank: %s' % (part[-1], rank)
		out =''

		if not part[-1] in self.high_ranks_alternative_ends:
			if not part[-1] == '0':	
				out += self.low_words[int( part[-1] )]
			if not int(part) == 0:
				out += ' ' + self.high_ranks[rank - 1]
				out += self.high_ranks_default_end
		else:
			out += self.low_words[int( part[-1] )]
			out += ' ' + self.high_ranks[rank - 1]
			out += self.high_ranks_alternative_ends[ part[-1] ]

		return out

