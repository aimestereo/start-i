#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from converter import converter

class Test(unittest.TestCase):
	def setUp(self):
		self.conv = converter()

	# tests for parse_dosens

	def test_parse_dosens_1(self):
		out = self.conv.parse_dosens('01', 2)
		self.assertEqual(out, u'один миллион')

	def test_parse_dosens_2(self):
		out = self.conv.parse_dosens('21', 3)
		self.assertEqual(out, u'двадцать один миллиард')

	def test_parse_dosens_2(self):
		out = self.conv.parse_dosens('36', 4)
		self.assertEqual(out, u'тридцать шесть триллионов')

	# tests for parse_thousands_last_digit
 	
	def test_parse_thousands_last_digit_1(self):
		out = self.conv.parse_thousands_last_digit('4')
		self.assertEqual(out, u'четыре тысячи')

	def test_parse_thousands_last_digit_2(self):
		out = self.conv.parse_thousands_last_digit('7')
		self.assertEqual(out, u'семь тысяч')

	def test_parse_thousands_last_digit_3(self):
		out = self.conv.parse_thousands_last_digit('3')
		self.assertEqual(out, u'три тысячи')

	# tests for parse_last_digit

	def test_parse_last_digit_1(self):
		out = self.conv.parse_high_ranks_last_digit('2', 2)
		self.assertEqual(out, u'два миллиона')

	def test_parse_last_digit_2(self):
		out = self.conv.parse_high_ranks_last_digit('8', 3)
		self.assertEqual(out, u'восемь миллиардов')

	def test_parse_last_digit_3(self):
		out = self.conv.parse_high_ranks_last_digit('3', 4)
		self.assertEqual(out, u'три триллиона')

	def test_parse_last_digit_4(self):
		out = self.conv.parse_thousands_last_digit('5')
		self.assertEqual(out, u'пять тысяч')

	def test_parse_last_digit_5(self):
		out = self.conv.parse_thousands_last_digit('7')
		self.assertEqual(out, u'семь тысяч')

	def test_parse_last_digit_6(self):
		out = self.conv.parse_thousands_last_digit('1')
		self.assertEqual(out, u'одна тысяча')


if __name__ == "__main__":
	unittest.main()