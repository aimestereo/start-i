#!/usr/bin/env python

import sys
if sys.version[0] == '2': input = raw_input		# 2.* and 3.* compatible
from converter import converter

while True:
	print 'Press "Enter" to exit.'
	text = input('Input number: ').replace(' ', '')
	if text == '': sys.exit(0)

	if not text.isdigit():
		print 'You should input correct int number'
		continue
	
	conv = converter()
	result = conv.convert(text)
	print "Result: %s" % (result)

