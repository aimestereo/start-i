# Test assignments

* * *
### General

[Python 2.7] (http://www.python.org/download/)
[Django 1.6.2] (https://www.djangoproject.com/download/1.6.2/tarball/)
[highcharts 3.0.9] (http://code.highcharts.com/zips/Highcharts-3.0.9.zip)

* * *
### Content:

1. [Test 1] (/vvkuznetsov/start-i/tree/master/test1)

2. [Test 2] (/vvkuznetsov/start-i/tree/master/test2)

* * *
There are test assignments from [Start-i] (http://www.start-i.ru/)
by Sulimov Alexandr <alexander.soulimov@start-i.ru>.