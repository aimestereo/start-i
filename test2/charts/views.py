import math
from django import http
from django.shortcuts import render
 
def data_source():
    '''Returns (x_value, y_value) tuples'''
#    return [(x, math.sin(x) for x in xrange(10)]		invalid sysntax
    result = []
    for x in xrange(10):
        result.append((x, math.sin(x)))

    return result
 
def index(request):
    context = { 'data': data_source() }
    return render(request, 'charts/index.html', context)

